A FreeCAD Workbench to aide in the modeling of US Stick Frame Construction. The workbench contains various, predefined, wooden construction members to speed the process of assembling the frame of a home or other wooden structure.

Installation
This Workbench is not available from the Installer in FreeCAD, you must place the files in your Mod directory.
For Linux:

    /home/username/.FreeCAD/Mod/ 

    For Windows:

    C:\Program Files\FreeCAD\Mod\

    For Mac:

    /Applications/FreeCAD/Mod/
For more details on installing a custom workbench:

    https://wiki.freecadweb.org/Installing_more_workbenches
